<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return [
  'db' => [
    'hostname' => '192.168.10.10',
    'port' => '3306',
    'driver' => 'Pdo_Mysql',
    'database' => 'preskok',
    'username' => 'homestead',
    'password' => 'secret',
  ],
//  'doctrine' => [
//    'connection' => [
//      'orm_default' => [
//        'driverClass' => PDOMySqlDriver::class,
//        'params' => [
//          'host'     => '192.168.10.10',
//          'user'     => 'homestead',
//          'password' => 'secret',
//          'dbname'   => 'preskok',
//        ]
//      ],
//    ],
//  ],
];

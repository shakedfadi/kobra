<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Parser;

use Parser\Controller\Factory\IndexControllerFactory;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'parser' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/parser',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
          'process-data' => [
            'type' => Literal::class,
            'options' => [
              'route'    => '/parser/process-data',
              'defaults' => [
                'controller' => Controller\IndexController::class,
                'action'     => 'process-data',
              ],
            ],
          ],
          'get-table-data' => [
            'type' => Literal::class,
            'options' => [
              'route'    => '/parser/get-table-data',
              'defaults' => [
                'controller' => Controller\IndexController::class,
                'action'     => 'get-table-data',
              ],
            ],
          ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => IndexControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'parser/index/index' => __DIR__ . '/../view/parser/index/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
          'ViewJsonStrategy',
        ],
    ],
];

<?php
namespace Parser\Service;

use Zend\View\Helper\Partial;
use Zend\View\Model\ModelInterface;
use Zend\View\View;

/**
 * Class ViewBuilderRenderer
 * @package Parser\Service
 */
class ViewBuilderRenderer
{
  /** @var View */
  protected static $view;

  /** @var Partial */
  protected static $partial;

  /** @var string */
  public static $wrapperTemplate = 'builder/wrapper.phtml';

  /**
   * @param View $view
   */
  public static function setView (View $view)
  {
    self::$view = $view;
  }

  public static function setPartial (Partial $partial)
  {
    self::$partial = $partial;
  }

  /**
   * @param string|ModelInterface $name
   * @param array|object $values
   * @return mixed
   */
  public static function renderPartial ($name = null, $values = null)
  {
    $partial = self::$partial;

    return $partial($name, $values);
  }

  public static function renderView (ModelInterface $model)
  {
    $view = self::$view;

    return $view->render($model);
  }
}
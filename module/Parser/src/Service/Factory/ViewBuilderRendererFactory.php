<?php
namespace Parser\Service\Factory;

use Parser\Service\ViewBuilderRenderer;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\View\Helper\Partial;
use Zend\View\View;

/**
 * Class ViewBuilderRendererFactory
 * @package Parser\Service\Factory
 */
class ViewBuilderRendererFactory implements FactoryInterface
{
  /**
   * @param ContainerInterface $container
   * @param string $requestedName
   * @param array|null $options
   * @return View
   */
  public function __invoke (ContainerInterface $container, $requestedName, array $options = null)
  {
    /** @var View $view */
    $view = $container->get('Zend\View\View');
    $renderer = $container->get('ViewRenderer');

    ViewBuilderRenderer::setView($view);

    $partial = new Partial();
    $partial->setView($renderer);

    ViewBuilderRenderer::setPartial($partial);

    return $view;
  }
}
<?php

namespace Parser\Service\Factory;

use Parser\Service\ParserService;
use Parser\Database\ParserDatabase;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\ServiceManager\Exception\ServiceNotFoundException;

class ParserServiceFactory implements FactoryInterface
{
  public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
  {
    if (!class_exists($requestedName))
      throw new ServiceNotFoundException(sprintf('Requested service name %s does not exists!', $requestedName));

    $parserDatabase = $container->get(ParserDatabase::class);

    return new ParserService($parserDatabase);
  }
}
<?php

namespace Parser\Service;

use Parser\Database\ParserDatabase;

class ParserService
{

  private $parserDatabase;

  public function __construct(ParserDatabase $parserDatabase) {
    $this->parserDatabase = $parserDatabase;
  }

  /**
   * Fetchin data
   * @param int $page
   * @param string $query
   * @return bool|\Zend\Paginator\Paginator
   */
  public function getData (int $page, string $query = '') {
    $result = $this->parserDatabase
      ->fetchAll($page, $query);

    return $result;
  }

  /**
   * Parse and insert data
   */
  public function processData ()
  {
    $rawData = file_get_contents('https://admin.b2b-carmarket.com//test/project');

    $string = trim(preg_replace('/\s+/', '', $rawData));
    $data = explode('<br>', $string);

    $dataArrays = [];
    foreach ($data as $row) {
      $dataArrays[] = explode(',', $row);
    }

    $titleRow = array_shift($dataArrays);
    $titleIndexes = array_flip($titleRow);

    $buyerIDs = [];
    foreach ($dataArrays as $array) {
      $buyerIDTitleIndex = $titleIndexes['BuyerID'];
      if (isset($array[$buyerIDTitleIndex])) {
        $buyerIDs[] = $array[$buyerIDTitleIndex];
      }
    }

    $buyerIDs = array_unique($buyerIDs);

    $this->parserDatabase->insertBuyerData($buyerIDs);
    $this->parserDatabase->insertData($dataArrays);
  }


}
<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Parser;


use Parser\Database\ParserDatabase;
use Parser\Service\Factory\ParserServiceFactory;
use Parser\Service\Factory\ViewBuilderRendererFactory;
use Parser\Service\ParserService;
use Parser\Service\ViewBuilderRenderer;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\ServiceManager\ServiceManager;

class Module implements ServiceProviderInterface
{
    const VERSION = '3.0.3-dev';

  /**
   * @var ServiceManager $serviceManager ;
   */
  private $serviceManager = null;

  public function onBootstrap(MvcEvent $e)
  {
    $sm = $this->serviceManager = $e->getApplication()->getServiceManager();
    $adapter = $sm->get('Zend\Db\Adapter\Adapter');
    \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::setStaticAdapter($adapter);
  }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
      return [
        'factories' => [
          ParserService::class => ParserServiceFactory::class,
          ParserDatabase::class => InvokableFactory::class,
          ViewBuilderRenderer::class => ViewBuilderRendererFactory::class
        ]
      ];
    }

}

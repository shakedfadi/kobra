<?php

namespace Parser\Database;

use Carbon\Carbon;
use Faker;
use Zend\Db;
use Zend\Paginator;

class ParserDatabase
{
  protected $db;

  protected function getAdapter ()
  {
    if (!$this->db)
      $this->db = Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();

    return $this->db;
  }

  /**
   * Insert to CisVehicle
   * @param array $dataArrays
   */
  public function insertData(array $dataArrays) {
    $adapter = $this->getAdapter();

    if (count($dataArrays)) {

      $truncate = $adapter->query('TRUNCATE TABLE `CisVehicle`');
      $truncate->execute();

      $con = $adapter->getDriver()->getConnection();
      $con->beginTransaction();
      try {
        foreach ($dataArrays as $row) {
          $stmt = $adapter->query("INSERT INTO `CisVehicle` (`VehicleID`, `InhouseSellerID`, `BuyerID`, `ModelID`, `SaleDate`, `BuyDate`) VALUES (?,?,?,?,?,?);");

          $stmt->execute($row);
        }
        $con->commit();
      } catch (Db\Exception\ErrorException $e) {
        $con->rollback();
      }
    }
  }

  /**
   * Insert to Buyer
   * @param array $buyerIDs
   */
  public function insertBuyerData(array $buyerIDs) {
    $adapter = $this->getAdapter();

    if (count($buyerIDs)) {
      $faker = Faker\Factory::create();

      $truncate = $adapter->query('TRUNCATE TABLE `Buyer`');
      $truncate->execute();

      $con = $adapter->getDriver()->getConnection();
      $con->beginTransaction();
      try {
        foreach ($buyerIDs as $buyerID) {
          $firstName = $faker->firstName;
          $lastName = $faker->lastName;

          $stmt = $adapter->query("INSERT INTO `Buyer` (`BuyerID`, `FirstName`, `Lastname`) VALUES (?,?,?);");

          $stmt->execute([(int) $buyerID, $firstName, $lastName]);
        }
        $con->commit();
      } catch (Db\Exception\ErrorException $e) {
        $con->rollback();
      }
    }
  }

  /**
   * Fetching data
   * @param int $page
   * @param string $query
   * @return bool|Paginator\Paginator
   */
  public function fetchAll(int $page, string $query) {
    $adapter = $this->getAdapter();

    $where = null;
    if ($query) {

      $dateCheck = date_parse($query);

      if ($dateCheck['year'] && $dateCheck['month'] && $dateCheck['day'])
        $query = Carbon::createFromDate($dateCheck['year'], $dateCheck['month'], $dateCheck['day'])->format('Y-m-d');

      $where = " WHERE `FirstName` LIKE '%{$query}%' OR `LastName` LIKE '%{$query}%' OR `SaleDate`= '{$query}'";
    }

    $stmt = $adapter->query("SELECT * FROM `CisVehicle` INNER JOIN `Buyer` ON Buyer.BuyerID = CisVehicle.BuyerID {$where}");

    $result = $stmt->execute();

    if ($result instanceof Db\Adapter\Driver\ResultInterface && $result->isQueryResult()) {
      $resultSet = new Db\ResultSet\ResultSet($result);
      $resultSet->initialize($result);
      $resultSet->buffer();
      $resultSet->count();
      $resultSet->toArray();

      $iteratorAdapter = new Paginator\Adapter\Iterator($resultSet);
      $paginator = new Paginator\Paginator($iteratorAdapter);
      $paginator->setItemCountPerPage(20);
      $paginator->setCurrentPageNumber($page);

      return $paginator;
    }

    return false;
  }

}
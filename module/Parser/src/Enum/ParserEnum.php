<?php

namespace Parser\Enum;

class ParserEnum
{
  const SALE_DATE_FIELD = 'SaleDate';
  const BUY_DATE_FIELD = 'BuyDate';

  const CIS_VEHICLE_FIELDS = [
    'VehicleID',
    'InhouseSellerID',
    'BuyerID',
    'FirstName',
    'LastName',
    'ModelID',
    self::SALE_DATE_FIELD,
    self::BUY_DATE_FIELD
  ];

  const DATE_FIELDS = [
    self::SALE_DATE_FIELD,
    self::BUY_DATE_FIELD
  ];
}
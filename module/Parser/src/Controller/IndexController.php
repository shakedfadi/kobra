<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Parser\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Parser\Service\ParserService;
use Parser\Service\ViewBuilderRenderer;
use Zend\View\View;

class IndexController extends AbstractActionController
{
  private $parserService;

  public function __construct(ParserService $parserService, View $viewBuilderRenderer)
  {
    $this->parserService = $parserService;
  }

  /**
   * Index displaying initial page
   * @return ViewModel
   */
  public function indexAction()
  {
    $page = (int) $this->params()->fromQuery('page', 1) ?? 1;
    $query = (string) $this->params()->fromQuery('query');

    $data = $this->parserService
      ->getData($page, $query);

    return new ViewModel([
      'result' => $data,
    ]);
  }

  /**
   * Getting data from paging and querying
   * @return JsonModel
   */
  public function getTableDataAction()
  {
    $page = (int) $this->params()->fromPost('page', 1) ?? 1;
    $query = (string) $this->params()->fromPost('query');

    $data = $this->parserService
      ->getData($page, $query);

    $pageCount = $data
      ->getPages()->pageCount;

    return new JsonModel([
      "Table" => $this->partial("parser/index/table", [
        'result' => $data
      ]),
      'PageCount' => $pageCount
    ]);
  }

  /**
   * Partial helper call
   * @param $template
   * @param array $variables
   * @return mixed
   */
  private function partial ($template, $variables = [])
  {
    return ViewBuilderRenderer::renderPartial($template, $variables);
  }

  /**
   * Processing new parsed data
   * @return JsonModel
   */
  public function processDataAction()
  {
    $this->parserService
      ->processData();

    return new JsonModel();
  }



}

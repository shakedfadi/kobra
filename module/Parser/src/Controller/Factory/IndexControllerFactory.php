<?php

namespace Parser\Controller\Factory;

use Parser\Service\ParserService;
use Parser\Controller\IndexController;
use Parser\Service\ViewBuilderRenderer;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Class IndexControllerFactory
 * @throws ServiceNotFoundException if unable to resolve the service.
 * @throws ServiceNotCreatedException if an exception is raised when
 *     creating a service.
 * @throws ContainerException if any other error occurs
 * @package Parser\Controller\Factory
 */
class IndexControllerFactory implements FactoryInterface {

  public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
  {
    if (!class_exists($requestedName))
      throw new ServiceNotFoundException(sprintf('Requested controller name %s does not exists!', $requestedName));

    $parserService = $container->get(ParserService::class);
    $viewBuilderRenderer = $container->get(ViewBuilderRenderer::class);

    return new IndexController($parserService, $viewBuilderRenderer);
  }
}


$(document).ready(function () {
    var container =  $(document).find('div.container.main');

    var content = container.find('section#table-content');
    var search = container.find("input[data-name='search-data']");
    var processDataButton = container.find("[data-name='parse-data']");

    var paginatorContent = container.find('section#paginator-content');
    var paginator = container.find("section#paginator-content ul#pagination");
    var pageCount = paginatorContent.data('page-count');
    var dataTableUrl = paginatorContent.data('table-url');
    var query = null;


    // Load data from url
    processDataButton.on('click', function (event) {
        event.preventDefault();
        var button = $(this);
        var url = button.attr('href');
        var indexUrl = button.data('index-url');

        $.post(url, {})
            .done(function() {
                location.href = indexUrl;
            });
    });

    paginator.twbsPagination({
        totalPages: pageCount,
        visiblePages: 10,
        initiateStartPageClick : false,
        onPageClick: function (event, page) {
            var pageParam = '?page=' + page;
            var searchParam = '&query=' + query;

            var param = (query) ?
                pageParam + searchParam :
                pageParam;

            history.pushState(null, null, param);
            fetchAndInsert(dataTableUrl, page, query);
            event.preventDefault();
        }
    });


    search.on('keyup', function (event) {
        event.preventDefault();
        if (event.keyCode == 13) {
            // Do something
            var input = $(this);
            var value = input.val();
            if (value == '') {
                location.href = processDataButton.data('index-url');
                return false;
            }

            var queryParam = '?query=' + value;
            query = value;

            history.pushState(null, null, queryParam);
            fetchAndInsert(dataTableUrl, null, value);
        }
    });

    // Get data for specific page and change table content
    function fetchAndInsert(url, number, query) {
        $.post(url, {page : number, query: query})
            .done(function(data) {
                pageCount = data.PageCount;
                content.html(data.Table);

                if (!pageCount)
                    paginator.twbsPagination('destroy');
            });

    }

    // Handle back/forward clicks
    $(window).on('popstate', function () {
        var param = location.search;
        var url = location.origin + location.pathname + '/get-table-data';

        query = null;
        var number = 1;
        if (param) {
            if (param.indexOf("?page=") >= 0) {
                var numberSubstring = param.substr(param.lastIndexOf("[?page=]") + 7);
                number = jQuery.isNumeric(numberSubstring) ? numberSubstring : numberSubstring.substr(0, numberSubstring.indexOf("&"));
            }

            if ((param.indexOf("?query=") >= 0) || (param.indexOf("&query=") >= 0)) {
                query = param.substr(param.lastIndexOf("query=") + 6)
            }
        }


        // var currentPage = paginator.twbsPagination('getCurrentPage');
        // console.log(currentPage);
        // paginator.twbsPagination('destroy');
        //
        // paginator.twbsPagination($.extend({}, defaultOpts, {
        //     startPage: number,
        //     totalPages: totalPages
        // }));

        // if (paginator.data("twbs-pagination")) {
        //     console.log('la');
        //     paginator.twbsPagination('destroy');
        // }
        //
        // paginator.twbsPagination({
        //     startPage: number,
        //     totalPages: pageCount,
        //     visiblePages: 10,
        //     initiateStartPageClick : false,
        //     onPageClick: function (event, page) {
        //         var pageParam = '?page=' + page;
        //         var searchParam = '&query=' + query;
        //
        //         console.log(page);
        //
        //         var param = (query) ?
        //             pageParam + searchParam :
        //             pageParam;
        //
        //         history.pushState(null, null, param);
        //         fetchAndInsert(dataTableUrl, page, query);
        //
        //         event.preventDefault();
        //     }
        // });


        fetchAndInsert(url, number, query);
    });
});
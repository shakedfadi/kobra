CREATE TABLE `CisVehicle` (
  VehicleID INT(11),
  InhouseSellerID INT(11),
  BuyerID INT(11),
  ModelID INT(11),
  SaleDate DATE,
  BuyDate DATE
);

CREATE TABLE `Buyer` (
  BuyerID INT(11),
  FirstName VARCHAR(30),
  LastName VARCHAR(30)
);
